[[_TOC_]]

# Objectives
- Understand the company, values, people & customers
- Understand what I can bring and how it can help me grow

# Team
## [Product Management Team](https://about.gitlab.com/company/team/?department=product-management)
52 People / 9 Non-US 
_Question: doesn't look like a geographically diverse team, why is that?_

# Tools
## Reliability - Incident Handling
[Pager Duty](https://www.youtube.com/watch?v=eGUjzjkOwSw&ab_channel=PagerDutyInc.)
## Wireframe & Prototyping:
[Figma](https://www.youtube.com/watch?v=eZJOSK4gXl4&ab_channel=KevinPowell)
[+GitLab Component Library](https://www.figma.com/community/file/781156790581391771)
Pajamas Design System: Single source of truth for the robust library of UI components that we use to build the GitLab product, including usage and implementation guidelines
## Hiring
[Greenhouse](https://www.youtube.com/watch?v=gC8lWDm6vW4&ab_channel=StitchFix%7CGreenhouseTutorials)
## HR
[BambooHR](https://www.youtube.com/watch?v=xrC2X73UQcU&ab_channel=TechnologyAdvice)
## Chat
### Slack
- Chatops bot : Interact with CI/CD jobs through Slack (e.g. feature flags)
- Danger bot:

# Software stack
- Ubuntu/Debian/CentOS/RHEL/OpenSUSE
- Ruby (MRI) 2.7.2
- Git 2.31+
- Redis 5.0+
- PostgreSQL 12+

# Gitlab Tiers
- Before Sept 2020, it had 2 different codebases for EE and CE, but moved to a single code base. With two, they had to merge to both, causing issues. Single code base work was the largest engineering work in gitlab to date.
- GitLab FOSS is a read-only mirror of GitLab, with all proprietary code removed
- User Types: Free,Trial, Consumption (Free + CI mins/storage $), Licensed (Premium/Ultimate)
- Delivery: Self Managed, SaaS
- About 10% of users are paid users (Mar.20). Gitlab generate almost all revenue with subscriptions to paid tiers
## Buyer Based Tiering
They make feature tiering decisions based on: **"Who cares most about the feature"** If the likely buyer is an individual contributor the feature will be open source, otherwise it will be source-available (proprietary).There aren't any features that are only useful to managers, directors, and executives. So for every source-available feature there will be an individual contributor who cares about it. Not saying that there aren't any individual contributors that care about the feature, just that they think that other buyers are relatively more likely to care about it.
|Self-Managed/SaaS tier|Free|Premium|Ultimate|
|----------------------|----|-------|--------|
Per user per month     |$0|$19|$99|
Who cares most about the feature|Individual Contributors|Directors|Executives|
Main competitor|GitHub Team|GitHub Enterprise|Veracode, Checkmarx|
Type of sell|No sell|Benefit/solution|Transformation|

When considering buyers as part of product tiering decisions they use the following guidance:
- Free is for a single developer, with the purchasing decision led by that same person
- Premium is for team(s) usage, with the purchasing decision led by one or more Directors
- Ultimate is for strategic organizational usage, with the purchasing decision led by one or more Execs

### Example template for customer response
**A customer asked why merge request approvals where not included in free:**

Thanks for asking. We think that managers are more likely to care about merge requests approvals than individual contributors. That doesn't mean that individual contributors don't care about them. And it doesn't mean that in all cases managers care more, just in most cases.

### If a feature can be moved down do it quickly
We should not hesitate to open source features. When we put a feature into too high of a tier we should move it quickly because not having it in all tiers limits who can use the feature and this hurts:
1. Getting more users: having the product sell itself makes us much more efficient, for it to sell itself it has to get used, more open source features can increase the popularity of GitLab compared to rival offerings.
2. Getting more stages per user: if an open source feature gets a user to adopt a new stage this increases the value one user gets from GitLab greatly since the single application benefits grow exponentially if you adopt more stages.
3. Getting more open source contributions to improve the feature: since the number of contributions grows with the number of users.
4. Increase demand for paid features on top of what you open source: When you move a feature down this increases the usage of the specific feature, for example our service desk functionality. When a feature is popular this creates more demand for paid features that we can add later like custom templates for service desks.

## Using Gitlab with uServices
A few ways GitLab simplifies microservice orchestration include…

1. Built-in CI/CD: GitLab’s built-in CI/CD is ideal for businesses looking to leverage microservices.
2. Built-in container registry and a robust Kubernetes integration: While microservices architecture can be used with legacy VM technology, containers and Kubernetes make building microservices significantly easier. GitLab is designed to work well with Kubernetes.
3. Built-in Monitoring: Monitoring is critical to a successful operation. GitLab’s monitoring capabilities leveraging Prometheus make GitLab ideal for microservices.
4. Multi-project pipelines support running pipelines with cross-project dependencies.
5. Monorepo support with the ability to run a pipeline only when code a specific directory changes.
6. Group-level Kubernetes clusters allow multiple projects to integrate with a single Kubernetes cluster.

# [Devops Market](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp.html)
## [Devops Value Stream Pain Points](https://about.gitlab.com/analysts/gartner-vsdp20/)
- Organizations find it difficult to scale their DevOps initiatives because of multiple point solutions that increase complexity due to orchestration, integration and management issues.
- Product teams struggle to reduce the time to market and deliver faster customer value due to constraints and lack of visibility into the flow of work.
- Platform teams find it challenging to set up scalable and secure access to DevOps environments as organizations transition to remote work styles while trying to reduce Infrastructure and Operational costs.
## Market Direction
- Microsoft acquired **GitHub** in 2018. GitHub introduced support for GitHub Actions.GitHub introduced Codespaces (in-browser integrated development environment [IDE] support) and GitHub Discussions
- **Digital.ai** was formed in 2020 through merger and acquisition of multiple companies that provided tools for specific phases of the DevOps life cycle. Digital.ai now positions itself as a single value stream platform by bringing together CollabNet VersionOne, XebiaLabs, Arxan, Experitest and Numerify.
- GitLab acquired Peach Tech and Fuzzit that help expand its support for fuzz testing and dynamic application security testing. GitLab acquired Gemnasium in January 2018 to build support for dependency scanning and container scanning. GitLab introduced “Auto DevOps” as a way to simplify creation of delivery pipelines without configuration.
- **CloudBees** acquired Electric Cloud (application release orchestration), Rollout.io (feature flag management) and CodeShip (container-native deployment). CloudBees continuous integration/continuous delivery (CI/CD) enabled by Jenkins X is an automated and opinionated CI/CD-as-a-service solution for cloud-native applications.
- Public Cloud Providers With Natively Supported VSDPs: The DevOps solution portfolio offered by public cloud providers gives organizations a clear path to build and deploy workloads to their cloud environments. However, their ease of deployment to other cloud environments has been a key constraint, thus limiting their widespread adoption in enterprises.
## [Future of Devops](https://about.gitlab.com/blog/2021/08/03/welcome-to-the-devops-platform-era/)
Monolith to microservice architecture move made the applications scale independently, allowing teams to move faster, faster delivery required companies to use more devops tools per project, more tools and more projects led to exponential increase in project-tool integrations, this is where gitlab shines. 
- Platform solution with embedded security _ is_ the future (security should shift left)
- Machine learning will be critical in making the DevOps workflow faster (e.g. testing and code review)
- DevOps platform adoption will accelerate.
## Gitlab’s Key Differentiators
- A platform that provides all the tools you need to move from ideation to delivery to learning.
- GitLab is infrastructure-agnostic–run in the cloud of your choice, on-premises, or through GitLab.com, our SaaS service
- Our single application, single data store and single permission model allows developers to focus less on maintaining integrations, and more on shipping value to their customers
## Value Proposition
- Development cost reduction
- Revenue gain due to decreased time to market
- Improved reliability and security
- Greater and more effective collaboration
- Emotional contribution of having delighted users
# Company
## Mission
Everyone can contribute: When everyone can contribute, users become contributors and we greatly increase the rate of innovation.
- To ensure that everyone can contribute with GitLab we allow anyone to create a proposal, at any time, without setup, and with confidence
- We actively welcome contributors to ensure that everyone can contribute to GitLab, the application
- To ensure that everyone can contribute to GitLab, the company we have open business processes. This allows all team members to suggest improvements to our handbook
## Strategy
### 3 Years
## [Tailwind Trends](https://about.gitlab.com/handbook/leadership/biggest-tailwinds/)
- Digital Transformation: Reduced cycle time
- Devops Adoption
- Multi Cloud:Not relying on single vendor
- All Remote

## Risks
The items toward the top of the list have a higher probability of occurring and/or anticipated level of consequence if are to occur
1. Security Breach
2. Gitlab.com reliability
3. Competition (Github is the biggest competitor)
4. Lack of performance management
5. Loss of the open source community
6. Key people leave
7. Ineffective Management
8. Loss of the values that bind us
9. Handbook Second

## Values
## Financials
150m$ ARR

# Product
## Personas
### Buyer Personas
- [App Dev Manager/Team Leader](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager)
- [App Dev Director/Architect](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director)
- [App Dev VP](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/buyer-persona/#erin---the-application-development-executive-vp-etc)
- CISO - Chief Information Security Officer
- TBD VP Infrastructure





# Other
## Visibility, Traceability, Auditability and Observability
The flow of work can be measured in terms of:
- Release velocity
- The number of aging work items
- Planned versus actual work
- Throughput: The number of work items completed in a set time period
- Lead time: The customer’s view of the elapsed time from requesting a capability to its release
- Cycle time: The elapsed time from when work is started to when it is completed
- Work in progress: The number of work items that have been started and remain either active or waiting
- Flow efficiency: The proportion of time that work items are active against their total cycle time
## Enterprise vs Consumer Software
Role based access control, auditing, ldap sign-in, saml sign-in etc, premium support, integrations, SLA, Deployment Options (private vs cloud), reporting&analytics, GDPR etc. 







